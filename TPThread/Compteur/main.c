#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

//Déclaration des variables
static int boucle = 1;
static int compteur = 0;

//Methode SIGINT
void SIGINTcompteur(){
  printf("SIGINT - compteur : ", compteur);
  compteur++;
}

//Methode SIGTERM
void SIGTERMfin(){
  printf("SIGTERM, fin");
  boucle = 0;
}

//Main programme
int main ()
{
    printf("Programme en cours d'execution");
    signal(SIGINT, SIGINTcompteur);
    signal(SIGTERM, SIGTERMfin);

    while (boucle == 1){}

    return 0;
}