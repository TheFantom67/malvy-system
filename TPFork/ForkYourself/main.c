#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

//Programme Main 
int main(void) {
  //Fork 
  pid_t pid = fork();
  
  //Process enfant
  if(pid == 0) {
    printf("Enfant => PPID: %d PID: %d\n", getppid(), getpid());
    exit(EXIT_SUCCESS);
  }
  //Process Parent
  else if(pid > 0) {
    printf("Parent => PID: %d\n", getpid());
    wait(NULL);
    printf("Processus enfant fini.\n");
  }
  //Message en cas d'echec
  else {
    printf("Impossible de creer le processus enfant.\n");
  }
 
  return EXIT_SUCCESS;
}