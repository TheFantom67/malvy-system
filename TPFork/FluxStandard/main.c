#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define STDOUT 1

//Programme Main 
int main(void) {
  //Fork 
  pid_t pid = fork();
  
  //Process enfant
  if(pid == 0) {
    printf("Enfant => PPID: %d PID: %d\n", getppid(), getpid());
    // Création fichier temporaire
    char template[] = "./tmp/proc-exercise";
    int fichier = mkstemp(template);
    //Dup2 sortie vers fichier
    dup2(fichier, STDOUT);
    //Affichage descripteur fichier
    printf("Id descripteur : ", fichier);
    //Fermeture fichier
    close(fichier);
    exit(EXIT_SUCCESS);
  }
  //Process Parent
  else if(pid > 0) {
    //Affiche le PID Parent
    printf("Parent => PID: %d\n", getpid());
    printf("Attente de la fin de processus fils\n");
    wait(NULL);
    printf("Processus enfant fini.\n");
    printf("Processus pere : That's all folks !!!\n");
  }
  //Message en cas d'echec
  else {
    printf("Impossible de creer le processus enfant.\n");
  }
 
  return EXIT_SUCCESS;
}