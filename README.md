# SYSTEMMALVY

MALVY Bruno

Voici les différents TP et Projet pour la matière Système d'exploitation.

TP SYSCALL - FORK - THREAD: (Temps: Beaucoup trop)

- Pour les TP SYSCALL, j'ai mis des morceaux de code que je souhaitai implémenter vis-à-vis des sujets proposés car je n'arrivai pas à compiler celui-ci ni à l'executer. 
- Pour les TP FORK, seul le "ForkYourSelf" et "Flux standard" ont été fait.
- Pour les TP Thread seul le "Compteur" à été fait.

TP DOCKER: (Temps: 4h)

- Pour le TP docker, celui-ci fonctionne.

PROJET DOCKER: (Temps: +24h)

Le  Projet DOCKER rencontre un problème à la création du conteneur DHCP lors de la récéption du nouveau fichier dhcpd.conf. Malgré tout, l'ensemble marche et communique ensemble. 

- Etude du sujet + Recherche (~4h)
- Préparation (~3h)
- Mise en place des services (~2h)
- Configuration des services (~4h)
- Réparation des bugs durant le projet(~5h)
- Récapitulatif .pdf (~3h)
- Autres (~3h)